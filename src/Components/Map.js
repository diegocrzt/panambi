import React, { Component } from "react";
import ReactDOM from "react-dom";
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import "./Map.css";
import cajerosData from "../cajeros";

class Map extends Component {
  componentDidMount() {
    const defaultLayer = L.tileLayer(
      "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
      {
        attribution:
          '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
      }
    );
    let map = (this.map = L.map(ReactDOM.findDOMNode(this), {
      minZom: 1,
      maxZoom: 20,
      layers: [defaultLayer],
      attributionControl: false
    }));

    map.on("click", this.onMapClick);
    map.on("locationfound", this.onLocationFound);
    map.fitWorld();
    map.setView([-25.37877231509495, -57.45059967041016], 13);

    let cajeros = L.layerGroup(
      cajerosData.data.map(obj => {
        const marker = L.marker([obj.latitud, obj.longitud], {
          icon: L.divIcon({})
        });
        marker.bindPopup(JSON.stringify(obj));
        return marker;
      })
    );

    L.control
      .layers({ default: defaultLayer }, { cajeros: cajeros })
      .addTo(map);
  }

  componentWillUnmount() {
    this.map.off("click", this.onMapClick);
    this.map = null;
  }

  onLocationFound(e) {
    let radius = e.accuracy / 2;

    L.marker(e.latlng)
      .addTo(this.map)
      .bindPopup("You are within " + radius + " meters from this point")
      .openPopup();

    L.circle(e.latlng, radius).addTo(this.map);
  }

  onMapClick(event) {
    //TODO: Here the magic ?
    console.log(event);
    const position = event.latlng;
    console.log(`Coordinates (lat,lng) = (${position.lat},${position.lng})`);
  }

  render() {
    return <div id="map" />;
  }
}

export default Map;
